//import 'dart:convert';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class DB {
  
  static final baseUrl = 'https://bbbae6c2.ngrok.io';

  static Future<String> login(email,password) async {
    var res = await http.post("$baseUrl/api/auth/login", body: {"email": email, "password": password});
    debugPrint(res.body.toString());
   if(res.statusCode == 200) return res.body.toString();
  return null;
  }
}
